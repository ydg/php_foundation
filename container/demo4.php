<?php 
/**
 * 外观模式:facade,也叫门面模式
 * 1.用一句来说:就是将操作进行封装,对外提供一个统一的接口
 * 2.因为操作可能分布在多个类中,而刚才学过的容器恰好可以将不同的类与实现封装起来
 * 3.所以外观模式与依赖容器是黄金搭档,经常会放在一起使用
 */

/**
 * 用户登录的操作涉及三个操作
 * 1.连接数据库
 * 2.用户数据验证
 * 3.输出提示信息
 */

require 'container.php';

//创建Facade类,实现以上三个功能
class Facade
{
	//连接数据库
	public static function connect(Container $container)
	{
		return $container->make('db')->connect();
	}

	//用户数据验证
	public static function check(Container $container)
	{
		return $container->make('validate')->check();
	}

	//输出提示信息
	public static function display(Container $container)
	{
		return $container->make('view')->display();
	}
}



//客户端调用
echo Facade::connect($container);
echo Facade::check($container);
echo Facade::display($container);


//可以在外观模型中使用初始化方法事先注入容器对象,来简化客户端调用
