<?php 

//数据库操作类
class Db
{
	//数据库连接
	public function connect()
	{
		return '数据库连接成功<br>';
	}
}

//数据验证类
class Validate
{
	//数据验证
	public function check()
	{
		return '数据验证成功<br>';
	}
}

//视图图
class View
{
	//内容输出
	public function display()
	{
		return '用户登录成功';
	}
}

/******************************************************************************/

//一.创建容器类
class Container
{
	//创建属性,用空数组初始化,该属性用来保存类与类的实例化方法
	public $instance = [];

	//初始化实例数组,将需要实例化的类,与实例化的方法进行绑定
	public function bind($abstract, Closure $process)
	{
		//键名为类名,值为实例化的方法
		$this->instance[$abstract] = $process;
	}

	//创建类实例
	public function make($abstract, $params=[])
	{
		return call_user_func_array($this->instance[$abstract],[]);
	}

}

/******************************************************************************/

//二、服务绑定: 将类实例注册到容器中
$container = new Container(); 

//将Db类绑定到容器中
$container->bind('db', function(){
	return new Db();
});

//将Validate类实例绑定到容器中
$container->bind('validate', function(){
	return new Validate();
});

//将View类实例绑定到容器中
$container->bind('view', function(){
	return new View();
});

