<?php 

//可以在外观模型中使用初始化方法事先注入容器对象,来简化客户端调用

require 'container.php';


class Facade
{
	//创建成员属性保存容器对象
	protected static $container = null;

	//创建初始化方法为容器对象赋值
	public static function initialize(Container $container)
	{
		static::$container = $container;
	}

	/**
	 * 因为已经在初始化方法中将容器对象导入到了当前类中,
	 * 并且保存到了类的静态属性中,为所有类成员所共享,
	 * 所以以下方法可直接调用不用再容器注入
	 * 注意:这里全部采用后期静态延迟绑定方法来访问当前容器对象
	 * 这主要是为了方便用户在静态继承的上下文环境中进行调用
	 */
	//连接数据库
	public static function connect()
	{
		return static::$container->make('db')->connect();
	}

	//用户数据验证
	public static function check()
	{
		return static::$container->make('validate')->check();
	}

	//输出提示信息
	public static function display()
	{
		return static::$container->make('view')->display();
	}
}



//客户端调用

//初始化类门面类中的容器对象
Facade::initialize($container);

//静态统一调用内部的方法(无须重复注入依赖容器对象啦,实现了细节隐藏,通用性更强)
echo Facade::connect();
echo Facade::check();
echo Facade::display();



