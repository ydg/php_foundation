<?php 
/**
 * 用户登录
 * 1.涉及数据库操作,数据验证,模板输出
 * 2.分别对应Db类,Validate类,View类
 * 3.仅做演示,具体实例请同学们自行完成
 */

//数据库操作类
class Db
{
	//数据库连接
	public function connect()
	{
		return '数据库连接成功<br>';
	}
}

//数据验证类
class Validate
{
	//数据验证
	public function check()
	{
		return '数据验证成功<br>';
	}
}

//视图图
class View
{
	//内容输出
	public function display()
	{
		return '用户登录成功';
	}
}

//用户类
class User
{
	//用户登录操作
	public function login()
	{
		//实例化Db类并调用connect()连接数据库
		$db = new Db();
		echo $db->connect();

		//实例化Validate类并调用check()进行数据验证
		$validate = new Validate();
		echo $validate->check();

		//实例化视图类并调用display()显示运行结果
		$view = new View();
		echo $view->display();
	}
}

/**
 * 知识:什么是客户端
 * 客户端:只要能发起请求,都可以看作客户端,浏览器,一段代码都可以
 * 以下的代码,在实例化User类,并调用其内部的loign方法进行工作
 * 所以,$user = new User();就是客户端代码
 * 或者,也可以这样理解,凡写在类或函数等代码之外的,都可看作客户端
 */
//创建User类
$user = new User();

//调用User对象的login方法进行登录操作
echo $user->login();

/**
 * 存在的问题:
 * 以上的四个类,只有User是实际工作类,其它三个都是工具类(Db,Validate,View)
 * 1.工作类中调用的工具类一旦发生变化,必须修改对这些工具类的所有引用代码,例如Db参数变化
 * 2.工作类的调用者必须对要用到的所有工具类,非常熟悉,对参数与返回值必须了解
 * 3.工作类对以上三个工具类,形成了严重的依赖,也叫类之间严重耦合
 *
 * 下面我们通过最常用的依赖注入(DI)来解藕
 */














