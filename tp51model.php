<?php
/**
 * TP5.1用到的开发模式：单例，工厂，注册树模式
 * Created by micai.
 * Author: 迷彩
 * Date: 2018/11/19
 * Time: 20:38
 */

//单例模式
Class Demo{
    //创建一个属性
    public $name = '';

    //创建类的静态实例
    protected static $instance = null;

    //禁用构造方法
    private function __construct($name="迷彩")
    {
        //初始化
        $this->name = $name;
    }

    //克隆方法私有化
    private function __clone()
    {
        //do something
    }

    //获得类的静态对象
    //（创建一个方法并不是他本来能做什么，
    //而是他本来什么都不能做，你需要让它做什么）
    public static function getInstance($name)
    {
        //判断是有当前类的实例化对象，没有则创建，有则返回
        if (!self::$instance instanceof self)
        {
            self::$instance=new self($name);
        }

        return self::$instance;
    }
}

//工厂模式
//使用工厂模式生成本类的单一实例
Class Factory
{
    //创建指定类的实例
    public static function create($name)
    {
        return Demo::getInstance($name);
    }
}

//注册树模式
/**
 *把对象挂到树上 set()
 *把对象从树上摘下来使用 get()
 *注销：_unset() 把摘下的对象吃掉
 */
Class Register
{
    //创建对象树 一个数组
    public static $objects = [];
    //$alias是对象的别名
    public static function set($alias,$object)
    {
        //将传进来的object对象挂到树上
        self::$objects[$alias] = $object;
    }

    //通过别名摘下对象使用
    public static function get($alias)
    {
        return self::$objects[$alias];
    }

    //把树上的对象摘下吃掉
    public static function _unset($alias)
    {
        unset(self::$objects[$alias]);
    }
}

Register::set('test',Factory::create('test'));
$obj = Register::get('test');
echo "<pre>";
print_r($obj);//返回的是一个对象
echo "</pre>";
var_dump($obj->name);