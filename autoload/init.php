<?php
/**
 * 类的的自动加载
 * User: micai
 * Date: 2018/3/3 0003
 * Time: 下午 3:39
 */
header('content-type:text/html;charset=utf8');
if(function_exists('spl_autoload_register')) {
    spl_autoload_register('myAutoLoad', true, true);
}

function myAutoLoad($className){
    require "./$className.class.php";
}
//function __autoload($className){
//    //自动加载类名为className,文件名为./$className.class.php的文件
//    require "./$className.class.php";
//}