<?php
/*
 * 初步了解hash，任何东西我们都可以hash它
 * 你都可以得到一个hash值。它只是对该数据进行数字签名。
 */
$list1 = ["a","b","c"];
$list2 = ["a","b","c"];
echo "list 1: ".md5(serialize($list1));
echo "<br/>list 2: ".md5(serialize($list2));

echo "<br>";

$list1 = ["aaa","b","c"];
$list2 = ["a","b","c"];
echo "list 1: ".md5(serialize($list1));
echo "<br/>list 2: ".md5(serialize($list2));