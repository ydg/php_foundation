<?php
/**
 * 以下所有代码请根据注释，分段执行
 * 这段代码主要讲解变量
 *变量:是用于存储信息的"容器"
 * 与代数类似，可以给 PHP 变量赋予某个值（x=5）或者表达式（z=x+y）。
 * 变量可以是很短的名称（如 x 和 y）或者更具描述性的名称（如 age、carname、totalvolume）。
 * PHP 变量规则：
 *  变量以 $ 符号开始，后面跟着变量的名称
 *  变量名必须以字母或者下划线字符开始
 *  变量名只能包含字母数字字符以及下划线（A-z、0-9 和 _ ）
 *  变量名不能包含空格
 *  变量名是区分大小写的（$y 和 $Y 是两个不同的变量）
 *  注意：PHP 语句和 PHP 变量都是区分大小写的。
 *
 * 入门的视频：http://www.runoob.com/php/php-tutorial.html
 */

    $x=5;
    $y=6;
    $z=$x+$y;
    echo $z;

    /*
     * 在上面的实例中，我们注意到，不必向 PHP 声明该变量的数据类型。
     *  PHP 会根据变量的值，自动把变量转换为正确的数据类型。
     *  在强类型的编程语言中，我们必须在使用变量前先声明（定义）变量的类型和名称。
     */

    /*
     * 变量的作用域是脚本中变量可被引用/使用的部分。
     * PHP 有四种不同的变量作用域：
     *  local/global/static/parameter
     */
    $x=5; // 全局变量

    function myTest()
    {
        $y=10; // 局部变量
        echo "<p>测试函数内变量:<p>";
        echo "变量 x 为: $x";
        echo "<br>";
        echo "变量 y 为: $y";
    }

    myTest();

    echo "<p>测试函数外变量:<p>";
    echo "变量 x 为: $x";
    echo "<br>";
    echo "变量 y 为: $y";

    /**
     *global 关键字用于函数内访问全局变量。
     *在函数内调用函数外定义的全局变量，我们需要在函数中的变量前加上 global 关键字：
     *
     */
    $x=5;
    $y=10;

    function myTest1()
    {
        global $x,$y;
        $y=$x+$y;
    }

    myTest1();
    echo $y; // 输出 15

    /**
     * PHP 将所有全局变量存储在一个名为 $GLOBALS[index] 的数组中。 index 保存变量的名称。这个数组可以在函数内部访问，也可以直接用来更新全局变量
     */
    $x=5;
    $y=10;

    function myTest2()
    {
        $GLOBALS['y']=$GLOBALS['x']+$GLOBALS['y'];
    }

    myTest2();
    echo $y;

    /**
     * Static 作用域
     *  当一个函数完成时，它的所有变量通常都会被删除。然而，有时候您希望某个局部变量不要被删除
     *  要做到这一点，请在您第一次声明变量时使用 static 关键字
     */
    function myTest3()
    {
        static $x=0;
        echo $x;
        $x++;
    }

    myTest3();
    myTest3();
    myTest3();

    /**
     * 参数作用域
     *  参数是通过调用代码将值传递给函数的局部变量
     *  参数是在参数列表中声明的，作为函数声明的一部分：
     */
    function myTest4($x)
    {
        echo $x;
    }
    myTest4(5);
?>