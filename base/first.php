<?php
/**
 * 获取服务器的信息
 * 第一个PHP程序
 * author：micai
 * date: 2018-11-20
 */
?>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <title>获取服务器信息</title>
    </head>
<body>
<?php
    $sysos = $_SERVER['SERVER_SOFTWARE']; //获取软件服务器标识的字符串
    $sysversion = PHP_VERSION; //获取PHP服务器版本

    //连接数据库并获取MYSQL版本信息
    $con=mysqli_connect("localhost","root","123456");
    // 检测连接
    if (mysqli_connect_errno($con))
    {
        echo "数据库连接失败: " . mysqli_connect_error();
    }
    $mysqlinfo = mysqli_get_server_info($con);
    mysqli_close($con);

    //从服务器获取GD库的信息
    if(function_exists("gd_info"))
    {
        $gd = gd_info();
        $gd_info = $gd["GD Version"];
    }
    else
    {
        $gd_info = '未知';
    }

    //从GD库中查看是否支持FreeType字体
    $freetype = $gd["FreeType Support"] ? "支持" : "不支持";

    //从PHP的配置文件中获得是否可以远程获取文件
    $allowurl = ini_get("allow_url_fopen") ? "支持" : "不支持";

    //从PHP配置中获取文件的最大上传限制
    $max_upload = ini_get("file_uploads") ? ini_get("upload_max_filesize") : "Disabled";

    //从PHP的配置文件获取脚本的最大执行时间
    $max_ex_time = ini_get("max_execution_time").'秒';

    //获取服务器时间，中国大陆采用的是东八区的时间，设置时区写成Etc/GMT-8
    date_default_timezone_set("Etc/GMT-8");
    $systime = date('Y-m-d H:i:s',time());

    /**
     * 以HTML表格的形式将以上获取到的服务器信息进行打印输出给客户端浏览器
     */
    echo "<table align='center' cellspacing='0' cellpadding='0'>";
    echo "<caption><h2>系统信息</h2></caption>";
    echo "<tr><td> web服务器：</td> <td>$sysos</td></tr>";
    echo "<tr><td> PHP版本：</td> <td>$sysversion</td></tr>";
    echo "<tr><td> Mysql版本：</td> <td>$mysqlinfo</td></tr>";
    echo "<tr><td> GD库版本：</td> <td>$gd_info</td></tr>";
    echo "<tr><td> FreeType：</td> <td>$freetype</td></tr>";
    echo "<tr><td> 远程文件获取：</td> <td>$allowurl</td></tr>";
    echo "<tr><td> 最大上传限制：</td> <td>$max_upload</td></tr>";
    echo "<tr><td> 最大执行时间：</td> <td>$max_ex_time</td></tr>";
    echo "<tr><td> 服务器时间：</td> <td>$systime</td></tr>";
    echo "</table>";

    /**
     *        系统信息
     *web服务器：	PhpStorm 2017.3
     *PHP版本：	7.2.12
     *Mysql版本：	5.6.17
     *GD库版本：	bundled (2.1.0 compatible)
     *FreeType：	支持
     *远程文件获取：	支持
     *最大上传限制：	2M
     *最大执行时间：	30秒
     *服务器时间：	2018-11-20 18:37:37
     **/

?>
</body>
</html>
