<?php
/**
 * Created by PhpStorm.
 * User: Micai
 * Date: 2018/3/3 0003
 * Time: 下午 2:57
 */
class MysqlDatabase{
    //数据库连接信息
    private $dbConfig=array(
        'host'=>'localhost',
        'port'=>'3306',
        'user'=>'root',
        'pwd'=>'root',
        'charset'=>'utf8',
        'dbname'=>'php',
    );
    //数据库连接资源
    private $link;
    //构造方法,私有化可以防止类在外部被实例化
    private function __construct($params=array())
    {
        //初始化属性
        $this->initAttr($params);
        //连接数数据库
        $this->connectServer();
        //设定字符集
        $this->setCharset();
    }
    private function initAttr($params){
        //初始化属性
        $this->dbConfig=array_merge($this->dbConfig,$params);
    }
    //获取数据库连接
    private function connectServer(){
        $host=$this->dbConfig['host'];
        $port=$this->dbConfig['port'];
        $user=$this->dbConfig['user'];
        $pwd=$this->dbConfig['pwd'];
        //连接数据库服务器
        if ($link=mysql_connect("$host:$port",$user,$pwd)){
            $this->link=$link;
        }else{
            die('数据库连接失败,请确认信息!'.mysql_error());
        }
    }
    //设定连接字符集
    private function setCharset(){
        $sql="set names {$this->dbConfig['charset']}";
        $this->query($sql);
    }
    //查询数据库显示错误信息
    function query($sql){
        if($result=mysql_query($sql)){
            //执行成功
            return $result;
        }else{
            //执行失败,显示错误信息以便于调试程序
            echo 'sql执行失败:<br>';
            echo '错误的sql为:',$sql,'<br>';
            echo '错误的代码为:',mysql_errno(),'<br>';
            echo '错误的信息为:',mysql_error(),'<br>';
            die();
        }
    }
//查询所有数据并返回结果集
    function fetchAll($sql){
        //执行query()函数
        if($result=query($sql)){
            //执行成功
            //遍历结果集
            $rows=array();
            while($row=mysql_fetch_array($result,MYSQL_ASSOC)){
                $rows[]=$row;
            }
            //释放结果集资源
            mysql_free_result($result);
            return $rows;
        }else{
            //执行失败
            return false;
        }
    }
//查询单条数据并返回结果集
    function fetchRow($sql){
        //执行query()函数
        if($result=query($sql)){
            //从结果集取得依次数据即可
            $row=mysql_fetch_array($result,MYSQL_ASSOC);
            return $row;
        }else{
            return false;
        }
    }
    //限制脚本实例化多个对象
    //单例对象调用
    private static $instance;
    //接下来私有化构造方法
    public static function getInstance($params=array()){
        //判断是否没有实例化过
        if(!self::$instance instanceof self){
            //如果未被实例化
            self::$instance=new self($params);
        }
        //返回对象
        return self::$instance;
    }
    //私有克隆,在外部调用克隆clone $对象名,会调用clone方法复制这个对象
    private function __clone()
    {
        // TODO: Implement __clone() method.
    }
}