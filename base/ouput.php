<?php
/**
 * echo 和 print 区别:
 *  echo - 可以输出一个或多个字符串
 *  print - 只允许输出一个字符串，返回值总为 1
 * 提示：echo 输出的速度比 print 快， echo 没有返回值，print有返回值1。
 * echo print 是一个语言结构，使用的时候可以不用加括号，也可以加上括号： echo 或 echo()/print或者print()。
 */
echo "<h2>PHP 很有趣!</h2>";
echo("PHP 很有趣!");
print "<h2>PHP 很有趣!</h2>";
print("<h2>PHP 很有趣!</h2>");
?>