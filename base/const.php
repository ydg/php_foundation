<?php
/**
 * 常量是一个简单值的标识符。该值在脚本中不能改变。
 * 一个常量由英文字母、下划线、和数字组成,但数字不能作为首字母出现。 (常量名不需要加 $ 修饰符)。
 * 注意： 常量在整个脚本中都可以使用。
 * bool define ( string $name , mixed $value [, bool $case_insensitive = false ] )
 * name：必选参数，常量名称，即标志符。
 * value：必选参数，常量的值。
 * case_insensitive ：可选参数，如果设置为 TRUE，该常量则大小写不敏感。默认是大小写敏感的。
 *
 */

// 区分大小写的常量名
define("GREETING", "欢迎访问 Runoob.com");
echo GREETING;    // 输出 "欢迎访问 Runoob.com"
echo '<br>';
echo greeting;   // 输出 "greeting"

//使用defined()判断常量是否存在
if(defined('GREETING'))
{
    echo GREETING;
}
?>