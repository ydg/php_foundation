<?php
/*
 * 导出大量的数据
 */
public function exportData()
{
    set_time_limit(0);
    ini_set('memory_limit', '1024M');
    $columns = [            '列名1', '列名2', '列名3'      //需要几列，定义好列名
    ];        //设置好告诉浏览器要下载excel文件的headers
    header('Content-Description: File Transfer');
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="导出数据-'.date('Y-m-d', time()).'.csv"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');        $fp = fopen('php://output', 'a');//打开output流
    mb_convert_variables('GBK', 'UTF-8', $columns);
    fputcsv($fp, $columns);//将数据格式化为CSV格式并写入到output流中

    //添加查询条件，获取需要的数据
    $query = Model::class()->where();

    //获取总数，分页循环处理
    $accessNum = $query->count();
    $perSize = 1000;
    $pages   = ceil($accessNum / $perSize);
    for($i = 1; $i <= $pages; $i++) {

        $db_data = $query->limit($perSize)->offset(($i-1)*$perSize)->get();
        foreach($db_data as $key => $value) {
            $rowData = [];                //获取每列数据，转换处理成需要导出的数据
            //需要格式转换，否则会乱码
            mb_convert_variables('GBK', 'UTF-8', $rowData);
            fputcsv($fp, $rowData);
        }            //释放变量的内存
        unset($db_data);            //刷新输出缓冲到浏览器
        ob_flush();            //必须同时使用 ob_flush() 和flush() 函数来刷新输出缓冲。
        flush();
    }

    fclose($fp);
    exit();
}