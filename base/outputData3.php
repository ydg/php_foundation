<?php
/*
 * 导出大量的数据到CSV
 */
$sql = 'select * from user';
$pdo = new \PDO('mysql:host=127.0.0.1;dbname=test', 'root', 'root');
$pdo->setAttribute(\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, false);
$rows = $pdo->query($sql);
$filename = date('Ymd') . '.csv'; //设置文件名
header('Content-Type: text/csv');
header("Content-Disposition: attachment;filename={$filename}");
$out = fopen('php://output', 'w');
fputcsv($out, ['id', 'username', 'password', 'create_time']);

foreach ($rows as $row) {
        $line = [$row['id'], $row['username'], $row['password'], $row['create_time']];

    fputcsv($out, $line);
}

fclose($out);
$memory = round((memory_get_usage() - $startMemory) / 1024 / 1024, 3) . 'M' . PHP_EOL;
file_put_contents('/tmp/test.txt', $memory, FILE_APPEND);
