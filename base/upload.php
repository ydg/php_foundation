<?php
/**
 * Created by PhpStorm.
 * User: Micai
 * Date: 2018/3/3 0003
 * Time: 上午 10:52
 */
class FileUpload{
    private $type=array(
        'image/jpeg','image/pjpeg','image/png','image/x-png','image/gif'
    );//文件类型
    public $maxSize=1048576;//1M=1024*1024,上传的容量
    public $uploadSavePath="./uploads"; //上传保存的路径
    public $errorMessage=""; //错误信息
    public function upload($file){
        //上传类首先得判断上传存不存在错误,错误信息有123467六种情况,0为正常上传
        if($file['error']>0){
            $errorMsg="错误信息为:";
            switch ($file['error']){
                case 1:$errorMsg.="文件大小超过了php.ini中upload_max_filesize选项限制的值";
                    break;
                case 2:$errorMsg.="文件大小超过了表单中max_file_size选项指定的值!";
                    break;
                case 3:$errorMsg.="文件只有部分被上传!";
                    break;
                case 4:$errorMsg.="没有文件被上传!";
                    break;
                case 6:$errorMsg.="找不到临时文件夹!";
                    break;
                case 7:$errorMsg.="临时文件写入失败";
                    break;
                default:$errorMsg.='未知错误!';
                    break;
            }
            return false;
        }
        //判断上传的文件是否属于$type内
        if(!in_array($file['type'],$this->type)){
            //不在所属类型内时
            $this->errorMessage="未定义的文件上传类型";
            return false;
        }
        //判断文件上传的大小不能超过所定义的大小
        if($file['size']>$this->maxSize){
            $this->errorMessage="超出上传所限制的最大上传容量";
            return false;
        }
        //给上传的图片重命名
        $newFileName=uniqid("php_").strrchr($file['name'],".");
        //设置上传文件的全目录 ./uploads/2018-03-03
        $allPath1=$this->uploadSavePath."/".date("Y-m-d");
        $allPath=$this->uploadSavePath."/".date("Y-m-d")."/".$newFileName;
        //判断这个目录是否存在
        if(!file_exists($allPath1)){
            mkdir($allPath1,'0777',true);
        }
        //移动
        if(!move_uploaded_file($file['tmp_name'],$allPath)){
            $this->errorMessage="文件上传失败";
        }else{
            return $allPath;
        };
    }
}